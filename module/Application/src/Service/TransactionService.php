<?php

namespace Application\Service;

use Application\Repository\TransactionRepository;
use Application\Repository\MarketingSourceRepository;
use Application\Repository\WrapUpStatusRepository;

use Application\Model\Transaction;
use Application\Model\FollowUpCampaign;

class TransactionService
{
    private $transactionRepository;
    private $marketingSourceRepository;
    private $wrapUpStatusRepository;

    public function __construct(
        TransactionRepository $transactionRepository,
        MarketingSourceRepository $marketingSourceRepository,
        WrapUpStatusRepository $wrapUpStatusRepository
    ) {
        $this->transactionRepository = $transactionRepository;
        $this->marketingSourceRepository = $marketingSourceRepository;
        $this->wrapUpStatusRepository = $wrapUpStatusRepository;
    }

    public function createTransaction(array $data)
    {
        $transactionToSave = new Transaction();

        $transactionToSave->createdByUserId = $data['createdByUserId'];

        $transactionToSave->vehicleYear = $data['vehicleYear'];
        $transactionToSave->vehicleMake = $data['vehicleMake'];
        $transactionToSave->vehicleModel = $data['vehicleModel'];
        $transactionToSave->vehicleTypeId = $data['vehicleTypeId'];

        $transactionToSave->vehicleHasTitle = $data['vehicleHasTitle'] == 'yes' ? true : false;

        // $transactionToSave->vehicleZipCode = $data['vehicleZipCode'];
        // $transactionToSave->vehicleCounty = $data['vehicleCounty'];

        $transactionToSave->offeredQuote = $data['offeredQuote'];

        $transactionToSave->sellerName = $data['sellerName'];
        $transactionToSave->sellerPhoneNumber = $data['sellerPhoneNumber'];
        $transactionToSave->sellerEmail = $data['sellerEmail'];

        $transactionToSave->marketingSourceId = $data['marketingSourceId'];
        $transactionToSave->leadSource =
            $this->marketingSourceRepository->get($data['marketingSourceId'])->description;

        $savedTransaction = $this->transactionRepository->saveTransaction($transactionToSave);

        return (object)[
            'isSuccess' => true,
            'savedTransaction' => $savedTransaction
        ];
    }

    public function updateTransaction(string $inspectionId, array $data)
    {
        $transactionToUpdate = $this->transactionRepository->get($inspectionId);

        if ($transactionToUpdate) {
            $transactionToUpdate->wrapUpStatusId = $this->wrapUpStatusRepository->getByDescription($data['callWrapUp'])->id;

            if ($data['callWrapUp'] === FollowUpCampaign::$ACCEPTED) {
                if (isset($data['vehicleStreetAddress']) && isset($data['vehicleCity']) && isset($data['vehicleZipCode'])) {
                    $transactionToUpdate->vehicleStreetAddress = $data['vehicleStreetAddress'];
                    $transactionToUpdate->vehicleCity = $data['vehicleCity'];
                    $transactionToUpdate->vehicleZipCode = $data['vehicleZipCode'];
                } else {
                    return (object)[
                        'isSuccess' => false,
                        'message' => 'Vehicle location info is required if the call wrap up is Accepted'
                    ];
                }
            }

            $updatedTransaction = $this->transactionRepository->updateTransaction($transactionToUpdate);

            return (object)[
                'isSuccess' => true,
                'updatedTransaction' => $updatedTransaction
            ];
        } else {
            return (object)[
                'isSuccess' => false,
                'message' => 'The transaction to update does not exist'
            ];
        }
    }

    public function getTransactionLog(array $filters)
    {
        return $this->transactionRepository->getAll($filters);
    }

    public function getTransactionLogCount(array $filters)
    {
        return $this->transactionRepository->getCount($filters);
    }
}
