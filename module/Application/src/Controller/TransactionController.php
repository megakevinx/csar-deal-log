<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\ServiceManager;

use Application\Controller\BaseRestfulController;

use Application\Repository\TransactionRepository;

use Application\Model\UserRole;

class TransactionController extends BaseRestfulController
{
    private $transactionRepository;

    public function __construct(TransactionRepository $transactionRepository)
    {
        $this->transactionRepository = $transactionRepository;
    }

    // GET api/transaction
    public function getList()
    {
        $data = $this->getRequest()->getQuery()->toArray();

        if (!$this->userIsRole(UserRole::DEAL_LOG_READER_ROLE) && !$this->userIsRole(UserRole::ADMIN_ROLE)) {
            return $this->notAuthorized();
        }

        return new JsonModel([
            'transactions' => $this->transactionRepository->getAll($data),
            'totalTransactionsCount' => $this->transactionRepository->getCount($data)
        ]);
    }
}
