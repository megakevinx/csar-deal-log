<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
//use Zend\Authentication\Result;
use Zend\Uri\Uri;

use Application\Controller\Form\LoginForm;

use Application\Service\AuthService;

use Application\Model\UserRole;

class AuthController extends AbstractActionController
{
    /**
     * Auth manager.
     * @var User\Service\authService
     */
    private $authService;
    
    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function loginAction()
    {
        // Create login form
        $form = new LoginForm();

        // Store login status.
        $isLoginError = false;

        // Check if user has submitted the form
        if ($this->getRequest()->isPost())
        {
            // Fill in the form with POST data
            $data = $this->params()->fromPost();

            $form->setData($data);

            // Validate form
            if ($form->isValid())
            {
                // Get filtered and validated data
                $data = $form->getData();

                // Perform login attempt.
                $result = $this->authService->login($data['email'], $data['password'], $data['remember_me']);

                // Check result.
                if ($result->isSuccess)
                {
                    // TODO: Add more redirects for each role
                    if ($result->identity->role === UserRole::DEAL_LOG_READER_ROLE)
                    {
                        return $this->redirect()->toRoute('home');
                    }
                    else if ($result->identity->role === UserRole::ADMIN_ROLE)
                    {
                        return $this->redirect()->toRoute('home');
                    }
                    else
                    {
                        $isLoginError = true;
                    }
                }
                else
                {
                    $isLoginError = true;
                }
            }
            else
            {
                $isLoginError = true;
            }
        }

        return new ViewModel([
            'form' => $form,
            'isLoginError' => $isLoginError
        ]);
    }
    
    /**
     * The "logout" action performs logout operation.
     */
    public function logoutAction() 
    {
        if ($this->identity())
        {
            $this->authService->logout();
        }

        return $this->redirect()->toRoute('login');
    }
}
