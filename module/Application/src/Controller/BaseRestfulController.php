<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class BaseRestfulController extends AbstractRestfulController
{
    protected function userIsRole($role)
    {
        $user = $this->identity();
        return $user && $user->role == $role;
    }

    protected function notAuthorized()
    {
        $this->getResponse()->setStatusCode(401);
        return new JsonModel([
            'status' => 401
        ]);
    }

    protected function badRequest($message)
    {
        $this->getResponse()->setStatusCode(400);
        return new JsonModel([
            'status' => 400,
            'message' => $message
        ]);
    }

    protected function methodNotAllowed()
    {
        $this->getResponse()->setStatusCode(405);
        return new JsonModel([
            'status' => 405
        ]);
    }
}
