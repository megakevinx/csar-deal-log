<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use Application\Repository\VehicleYearRepository;
use Application\Repository\VehicleMakeRepository;

use Application\Model\UserRole;

class HomeController extends AbstractActionController
{
    const ADMIN_ROLE = "Administrator";
    const DEAL_LOG_READER_ROLE = "DealLogReader";

    private $vehicleYearRepository;
    private $vehicleMakeRepository;

    public function __construct(VehicleYearRepository $vehicleYearRepository, VehicleMakeRepository $vehicleMakeRepository)
    {
        $this->vehicleYearRepository = $vehicleYearRepository;
        $this->vehicleMakeRepository = $vehicleMakeRepository;
    }

    public function indexAction()
    {
        $user = $this->identity();

        if (!$user || !in_array($user->role, [UserRole::DEAL_LOG_READER_ROLE, UserRole::ADMIN_ROLE])) {
            return $this->redirect()->toRoute('login');
        }

        return new ViewModel([
            'vehicleYears' => $this->vehicleYearRepository->getAll(),
            'vehicleMakes' => $this->vehicleMakeRepository->getAll()
        ]);
    }
}
