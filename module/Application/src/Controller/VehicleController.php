<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\Http\PhpEnvironment\Request;
use Zend\ServiceManager\ServiceManager;

use Application\Controller\BaseRestfulController;

use Application\Controller\Form\GetModelsForm;

use Application\Repository\VehicleModelRepository;

class VehicleController extends BaseRestfulController
{
    private $vehicleModelRepository;

    public function __construct(VehicleModelRepository $vehicleModelRepository)
    {
        $this->vehicleModelRepository = $vehicleModelRepository;
    }

    // GET api/vehicle/models?params...
    public function modelsAction()
    {
        if ($this->getRequest()->isGet())
        {
            $data = $this->getRequest()->getQuery()->toArray();

            $form = new GetModelsForm();
            $form->setData($data);

            if ($form->isValid())
            {
                $data = $form->getData();

                $models = [];

                if ($data['year'])
                {
                    $models = $this->vehicleModelRepository->getByYearAndMake($data['year'], $data['make']);
                }
                else
                {
                    $models = $this->vehicleModelRepository->getByMake($data['make']);
                }

                return new JsonModel([
                    'models' => $models
                ]);
            }
            else
            {
                return $this->badRequest("Input data invalid");
            }
        }
        else
        {
            $this->methodNotAllowed();
        }
    }
}
