<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Authentication\AuthenticationService;
use Zend\Session\SessionManager;
use Zend\Authentication\Storage\Session as SessionStorage;

class Module implements ConfigProviderInterface
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function getServiceConfig()
    {
        return [
            'factories' => [
                // Services

                Service\AuthService::class => function($container) {

                    // Get contents of 'access_filter' config key (the AuthManager service
                    // will use this data to determine whether to allow currently logged in user
                    // to execute the controller action or not.
                    // $config = $container->get('Config');
                    // if (isset($config['access_filter']))
                    //     $config = $config['access_filter'];
                    // else
                    //     $config = [];

                    return new Service\AuthService(
                        $container->get(\Zend\Authentication\AuthenticationService::class),
                        $container->get(\Zend\Session\SessionManager::class),
                        $container->get(Repository\UserRepository::class)
                    );
                },
                \Zend\Authentication\AuthenticationService::class => function($container) {
                    return new AuthenticationService(
                        new SessionStorage(
                            'Zend_Auth', 'session', $container->get(\Zend\Session\SessionManager::class)
                        ),
                        $container->get(Service\AuthAdapter::class)
                    );
                },
                Service\AuthAdapter::class => function($container) {
                    return new Service\AuthAdapter(
                        $container->get(Repository\UserRepository::class)
                    );
                },

                // Repositories
                Repository\TransactionRepository::class => function($container) {
                    $transactionTableGateway = $container->get(Model\TransactionTableGateway::class);
                    $inspectionTableGateway = $container->get(Model\InspectionTableGateway::class);
                    return new Repository\TransactionRepository($transactionTableGateway, $inspectionTableGateway);
                },
                Repository\UserRepository::class => function($container) {
                    return new Repository\UserRepository(
                        $container->get(AdapterInterface::class),
                        $container->get(Model\UserTableGateway::class),
                        $container->get(Model\RoleTableGateway::class),
                        $container->get(Model\UserRoleAppTableGateway::class)
                    );
                },
                Repository\VehicleYearRepository::class => function($container) {
                    return new Repository\VehicleYearRepository();
                },
                Repository\VehicleMakeRepository::class => function($container) {
                    return new Repository\VehicleMakeRepository(
                        $container->get(Model\VehicleMakeTableGateway::class)
                    );
                },
                Repository\VehicleModelRepository::class => function($container) {
                    return new Repository\VehicleModelRepository(
                        $container->get(Model\VehicleModelTableGateway::class)
                    );
                },

                // Table Gateways
                Model\TransactionTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Transaction());
                    return new TableGateway('Transaction', $dbAdapter, null, $resultSetPrototype);
                },
                Model\InspectionTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    return new TableGateway('Inspection', $dbAdapter, null, $resultSetPrototype);
                },
                Model\UserTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User', $dbAdapter, null, null);
                },
                Model\RoleTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('UserRole', $dbAdapter, null, null);
                },
                Model\UserRoleAppTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    return new TableGateway('User_UserRole_Application', $dbAdapter, null, null);
                },
                Model\VehicleMakeTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleMake());
                    return new TableGateway('VehicleMake', $dbAdapter, null, $resultSetPrototype);
                },
                Model\VehicleModelTableGateway::class => function($container) {
                    $dbAdapter = $container->get(AdapterInterface::class);
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\VehicleModel());
                    return new TableGateway('VehicleModel', $dbAdapter, null, $resultSetPrototype);
                },
            ],
        ];
    }

    public function getControllerConfig()
    {
        return [
            'factories' => [

                Controller\AuthController::class => function($container) {
                    return new Controller\AuthController(
                        $container->get(Service\AuthService::class)
                    );
                },

                Controller\TransactionController::class => function($container) {
                    return new Controller\TransactionController(
                        $container->get(Repository\TransactionRepository::class)
                    );
                },

                Controller\HomeController::class => function($container) {
                    return new Controller\HomeController(
                        $container->get(Repository\VehicleYearRepository::class),
                        $container->get(Repository\VehicleMakeRepository::class)
                    );
                },

                Controller\VehicleController::class => function($container) {
                    return new Controller\VehicleController(
                        $container->get(Repository\VehicleModelRepository::class)
                    );
                },

            ],
        ];
    }
}
