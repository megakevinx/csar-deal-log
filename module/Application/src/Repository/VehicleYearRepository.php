<?php

namespace Application\Repository;

class VehicleYearRepository
{
    private static $EARLIEST_YEAR = 1990;

    public function getAll()
    {
        $result = array();

        for ($i = VehicleYearRepository::$EARLIEST_YEAR; $i <= date("Y", time()); $i++) {
            $result[] = $i;
        }

        return $result;
    }
}