<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\Transaction;

// TODO: Refactor magic strings for column names
class TransactionRepository
{
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    private $transactionTableGateway;
    private $inspectionTableGateway;

    public function __construct(TableGatewayInterface $transactionTableGateway, TableGatewayInterface $inspectionTableGateway)
    {
        $this->transactionTableGateway = $transactionTableGateway;
        $this->inspectionTableGateway = $inspectionTableGateway;
    }

    private function applyTransactionLogSearchFilters(Select $select, array $filters)
    {
        if ($filters['searchDealNumber'] ?? null) {
            $select->where->equalTo('Inspection_ID', $filters['searchDealNumber']);
        }
        if ($filters['searchVin'] ?? null) {
            $select->where->like('VIN', '%' . $filters['searchVin'] . '%');
        }

        if ($filters['searchFromDate'] ?? null) {
            $select->where->greaterThanOrEqualTo('DateVal', $filters['searchFromDate']);
        }
        if ($filters['searchToDate'] ?? null) {
            $select->where->lessThanOrEqualTo('DateVal', $filters['searchToDate']);
        }

        if ($filters['searchVehicleYear'] ?? null) {
            $select->where->equalTo('_Year', $filters['searchVehicleYear']);
        }
        if ($filters['searchVehicleMake'] ?? null) {
            $select->where->equalTo('Make', $filters['searchVehicleMake']);
        }
        if ($filters['searchVehicleModel'] ?? null) {
            $select->where->equalTo('Model', $filters['searchVehicleModel']);
        }

        return $select;
    }

    public function getAll(array $filters)
    {
        $pageIndex = (int)$filters['pageIndex'];
        $pageSize = (int)$filters['pageSize'];

        $dbAdapter = $this->transactionTableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('Inspection')->columns(['Inspection_ID', 'DateVal', '_Year', 'Make', 'Model', 'Color', 'VIN', 'KeysLoc', 'Wheels', 'Doors', 'TitleNum', 'Title'])
               ->where(['Cancelled' => 0, 'Incomplete' => 0])
               ->order('DateVal DESC');

        $this->applyTransactionLogSearchFilters($select, $filters);

        $select->limit($pageSize)->offset((($pageIndex - 1) * $pageSize));

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results) {
            $transactions = [];

            foreach ($results as $row) {
                $transaction = new Transaction();

                $transaction->exchangeArray($row);

                $transactions[] = $transaction;
            }

            return $transactions;
        } else {
            return null;
        }
    }

    public function getCount(array $filters)
    {
        $dbAdapter = $this->transactionTableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $sql->select();
        $select->from('Inspection')->columns(['Inspection_ID', 'DateVal', '_Year', 'Make', 'Model', 'Color', 'VIN', 'KeysLoc', 'Wheels', 'Doors', 'TitleNum', 'Title'])
            ->where(['Cancelled' => 0, 'Incomplete' => 0]);

        $select->columns([ 'num' => new \Zend\Db\Sql\Expression('COUNT(*)') ]);

        $this->applyTransactionLogSearchFilters($select, $filters);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        return (int)$results[0]['num'];
    }
}
