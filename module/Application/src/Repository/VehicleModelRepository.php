<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

use Application\Model\VehicleModel;

class VehicleModelRepository
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getByYearAndMake(string $vehicleYear, string $vehicleMakeId)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $sql = new Sql($dbAdapter);

        $select = $sql->select()
            ->from('VehicleModelYear')->columns(['Year', 'VehicleModel_ID', 'VehicleMake_ID'])
            ->join(
                'VehicleModel',
                'VehicleModelYear.VehicleModel_ID = VehicleModel.VehicleModel_ID AND VehicleModelYear.VehicleMake_ID = VehicleModel.VehicleMake_ID',
                ['Name']
            )
            ->where([
                'VehicleModelYear.Year' => $vehicleYear,
                'VehicleModelYear.VehicleMake_ID' => $vehicleMakeId
            ])
            ->order('VehicleModel.Name');

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        if ($results)
        {
            $models = [];

            foreach ($results as $row)
            {
                $model = new VehicleModel();
                $model->exchangeArray($row);

                $models[] = $model;
            }

            return $models;
        }
        else
        {
            return null;
        }
    }
}