<?php

namespace Application\Repository;

use RuntimeException;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\TableGatewayInterface;

use Application\Model\User;

class UserRepository
{
    const APP_CODE = 'DEAL_LOG_APP';

    private $dbAdapter;
    private $userTableGateway;
    private $roleTableGateway;
    private $userRoleAppTableGateway;

    public function __construct(Adapter $dbAdapter, TableGatewayInterface $userTableGateway, TableGatewayInterface $roleTableGateway, TableGatewayInterface $userRoleAppTableGateway)
    {
        $this->dbAdapter = $dbAdapter;
        $this->userTableGateway = $userTableGateway;
        $this->roleTableGateway = $roleTableGateway;
        $this->userRoleAppTableGateway = $userRoleAppTableGateway;
    }

    private function select($where)
    {
        $sql = new Sql($this->dbAdapter);

        $select = $sql->select()
            ->from('User')->columns(['User_ID', 'Email', 'Password', 'FirstName', 'LastName', 'PhoneNumber'])
            ->join('User_UserRole_Application', 'User.User_ID = User_UserRole_Application.User_ID')
            ->join('UserRole', 'User_UserRole_Application.UserRole_ID = UserRole.UserRole_ID', ['Role' => 'Description'])
            ->where($where);

        $selectString = $sql->getSqlStringForSqlObject($select);
        $results = $this->dbAdapter->query($selectString, Adapter::QUERY_MODE_EXECUTE)->toArray();

        return $results;
    }

    private function createUserObject($userData)
    {
        if ($userData)
        {
            $user = new User();
            $user->exchangeArray($userData[0]);

            return $user;
        }
        else
        {
            return null;
        }
    }

    public function get(int $id)
    {
        $results = $this->select(['User.User_ID' => $id]);
        return $this->createUserObject($results);
    }

    public function getByEmail(string $email)
    {
        $results = $this->select([
            'User.Email' => $email,
            'User_UserRole_Application.Application_Code' => self::APP_CODE
        ]);

        return $this->createUserObject($results);
    }
}