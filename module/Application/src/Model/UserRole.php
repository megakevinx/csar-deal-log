<?php

namespace Application\Model;

abstract class UserRole
{
    const ADMIN_ROLE = "Administrator";
    const DEAL_LOG_READER_ROLE = "DealLogReader";
}
