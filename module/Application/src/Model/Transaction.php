<?php

namespace Application\Model;

class Transaction
{
    public $inspectionId;
    public $date;
    public $vehicleYear;
    public $vehicleMake;
    public $vehicleModel;
    public $vehicleColor;
    public $vehicleVin;
    public $vehicleKeysLocation;
    public $vehicleWheels;
    public $vehicleDoors;
    public $vehicleHasTitle;
    public $vehicleTitleNumber;

    // In order to work with zend-db's TableGateway class, we need to implement this method.
    public function exchangeArray(array $data)
    {
        $this->inspectionId = $data['Inspection_ID'] ?? null;
        $this->date = $data['DateVal'] ?? null;
        $this->vehicleYear = $data['_Year'] ?? null;
        $this->vehicleMake = $data['Make'] ?? null;
        $this->vehicleModel = $data['Model'] ?? null;
        $this->vehicleColor = $data['Color'] ?? null;
        $this->vehicleVin = $data['VIN'] ?? null;
        $this->vehicleKeysLocation = $data['KeysLoc'] ?? null;
        $this->vehicleWheels = $data['Wheels'] ?? null;
        $this->vehicleDoors = $data['Doors'] ?? null;
        $this->vehicleHasTitle = $data['Title'] ?? null;
        $this->vehicleTitleNumber = $data['TitleNum'] ?? null;
    }
}
