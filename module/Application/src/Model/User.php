<?php

namespace Application\Model;

class User
{
    public $id;

    public $email;
    public $password;

    public $firstName;
    public $lastName;
    public $phoneNumber;
    public $companyName;
    public $companyAddress;
    public $paymentMethod;

    public $role;

    public function exchangeArray(array $data)
    {
        $this->id = !empty($data['User_ID']) ? $data['User_ID'] : null;

        $this->email = !empty($data['Email']) ? $data['Email'] : null;
        $this->password = !empty($data['Password']) ? $data['Password'] : null;

        $this->firstName = !empty($data['FirstName']) ? $data['FirstName'] : null;
        $this->lastName = !empty($data['LastName']) ? $data['LastName'] : null;
        $this->phoneNumber = !empty($data['PhoneNumber']) ? $data['PhoneNumber'] : null;
        $this->companyName = !empty($data['CompanyName']) ? $data['CompanyName'] : null;
        $this->companyAddress = !empty($data['CompanyAddress']) ? $data['CompanyAddress'] : null;
        $this->paymentMethod = !empty($data['PaymentMethod']) ? $data['PaymentMethod'] : null;

        $this->role = !empty($data['Role']) ? $data['Role'] : null;
    }
}