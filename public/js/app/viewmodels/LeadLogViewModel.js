define(
    [
        'knockout', 'jquery', 'lodash', 'bootstrap', 'bootstrap-datepicker',
        'app/alerts/AlertsManager',
        'app/apiclients/TransactionApiClient',
        'app/apiclients/VehicleMakeApiClient'
    ],

    function (ko, $, _, bootstrap, bsDatepicker,
              AlertsManager,
              TransactionApiClient,
              VehicleMakeApiClient) {

        function LeadLogViewModel(alertsManager, transactionApiClient, vehicleMakeApiClient) {

            var self = this;

            self.alertsManager = alertsManager;
            self.transactionApiClient = transactionApiClient;
            self.vehicleMakeApiClient = vehicleMakeApiClient;

            self.vehicleModels = ko.observableArray();

            self.isLoadingRequest = ko.observable(false);

            self.searchDealNumber = ko.observable();
            self.searchVin = ko.observable();
            self.searchFromDate = ko.observable();
            self.searchToDate = ko.observable();
            self.searchVehicleYear = ko.observable();
            self.searchVehicleMake = ko.observable();
            self.searchVehicleModel = ko.observable();

            self.transactions = ko.observable();

            self.pageSize = ko.observable(10);
            self.pageIndex = ko.observable(1);

            self.pageSizes = [10, 50, 100, 500];
            self.totalTransactionsCount = ko.observable(0);

            self.morePagesUp = ko.observable(false);
            self.morePagesDown = ko.observable(false);

            self.numberOfPages = ko.pureComputed(function () {
                return self.totalTransactionsCount() / self.pageSize();
            });
        
            self.dataPages = ko.pureComputed(function () {
                return _.range(1, self.numberOfPages() + 1);
            });

            self.dataPagesToShow = ko.pureComputed(function () {
                if (self.numberOfPages() > 10) {
                    if (self.pageIndex() <= 5) {
                        self.morePagesUp(true);
                        self.morePagesDown(false);
                        return _.range(1, 11);
                    }
                    else if (self.pageIndex() > self.numberOfPages() - 6) {
                        self.morePagesUp(false);
                        self.morePagesDown(true);
                        return _.range(self.pageIndex() - 4, self.numberOfPages() + 1);
                    }
                    else {
                        self.morePagesUp(true);
                        self.morePagesDown(true);
                        return _.range(self.pageIndex() - 4, self.pageIndex() + 6);
                    }
                }
                else {
                    self.morePagesUp(false);
                    self.morePagesDown(false);
                    return _.range(1, self.numberOfPages() + 1);
                }
            });
        
            self.showNextPage = function() {
                var newPageIndex = self.pageIndex();

                if (!(self.pageIndex() >= self.numberOfPages())) {
                    newPageIndex = (self.pageIndex() + 1);
                }

                self.showPage(newPageIndex);
            };
        
            self.showPreviousPage = function() {
                var newPageIndex = self.pageIndex();

                if (!(self.pageIndex() <= 1)) {
                    newPageIndex = (self.pageIndex() - 1);
                }

                self.showPage(newPageIndex);
            };
        
            self.pageSizeOnSelect = function () {
                var newPageIndex = 1;
                self.showPage(newPageIndex);
            }
        
            self.showPage = function (selectedPageIndex) {
                self.pageIndex(selectedPageIndex);
                self.search();
            }

            self.vehicleYearOnSelect = function() {
                loadVehicleModels();
            };

            self.vehicleMakeOnSelect = function() {
                loadVehicleModels();
            };

            function loadVehicleModels() {
                if (self.searchVehicleYear() && self.searchVehicleMake()) {
                    self.vehicleMakeApiClient.getVehicleModels({

                        vehicleMake: self.searchVehicleMake(),
                        vehicleYear: self.searchVehicleYear(),

                        onSuccess: function (response) {
                            var models = _.map(response.models, function(value) {
                                return { id: value.name.toLowerCase(), name: value.name };
                            });

                            self.vehicleModels(models);
                        },
                        onFail: alertsManager.onAjaxRequestFail
                    });
                }
            }

            self.searchOnClick = function () {
                self.showPage(1);
            }

            self.search = function() {

                var searchFilters = {
                    pageIndex: self.pageIndex(),
                    pageSize: self.pageSize(),
                    searchDealNumber: self.searchDealNumber(),
                    searchVin: self.searchVin(),
                    // TODO: WTF. For some reason the observable is not updated. Bootstrap datepicker conflict?
                    searchFromDate: $("[name='search_date_from']").val(),
                    searchToDate: $("[name='search_date_to']").val(),
                    //ko_searchFromDate: self.searchFromDate(),
                    //ko_searchToDate: self.searchToDate(),
                    searchVehicleYear: self.searchVehicleYear(),
                    searchVehicleMake: self.searchVehicleMake(),
                    searchVehicleModel: self.searchVehicleModel()
                };

                self.isLoadingRequest(true);

                transactionApiClient.getTransactionLog({
                    searchFilters: searchFilters,
                    onSuccess: function (response) {

                        self.isLoadingRequest(false);

                        if (!response.transactions) {
                            alertsManager.showWarning(
                                $("#alerts_container"),
                                "No transactions found"
                            );

                            self.transactions(null);
                            self.totalTransactionsCount(0);
                        }
                        else {
                            self.transactions(response.transactions);
                            self.totalTransactionsCount(response.totalTransactionsCount);
                        }
                    },
                    onFail: function(jqXHR, textStatus, error) {
                        self.isLoadingRequest(false);
                        alertsManager.onAjaxRequestFail(jqXHR, textStatus, error);
                    } 
                });
            };
        }

        $(document).ready(function() {

            $("div.validation-error-alert a.close").click(function(eventObject) {
                $(eventObject.target.parentElement).hide();
            });

            $('.date-input-field').datepicker({
                 format: "yyyy-mm-dd"
            });

            ko.applyBindings(new LeadLogViewModel(
                new AlertsManager(),
                new TransactionApiClient(),
                new VehicleMakeApiClient()
            ));

            $("#overlay").fadeOut();
         });
    }
);