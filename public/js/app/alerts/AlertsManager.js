define('app/alerts/AlertsManager',
    ['jquery'],
    function($) {
        function AlertsManager() {

            self = this;

            var alertTypes = {
                success: "success",
                info: "info",
                warning: "warning",
                danger: "danger"
            };

            var alertPattern = 
                '<div id="{ALERT_ID}" class="alert alert-{ALERT_TYPE} alert-dismissable">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + 
                    "{ALERT_MESSAGE}" +
                '</div>'

            function show(alertContainer, alertType, alertMessage, alertId) {

                alertId = alertId || '';

                alertContainer.append(
                    alertPattern
                        .replace("{ALERT_TYPE}", alertType)
                        .replace("{ALERT_MESSAGE}", alertMessage)
                        .replace("{ALERT_ID}", alertId)
                );
            }

            function showSuccess(alertContainer, alertMessage) {
                show(alertContainer, alertTypes.success, alertMessage);
            }

            function showInfo(alertContainer, alertMessage) {
                show(alertContainer, alertTypes.info, alertMessage);
            }

            function showWarning(alertContainer, alertMessage) {
                show(alertContainer, alertTypes.warning, alertMessage);
            }

            function showDanger(alertContainer, alertMessage, alertId) {

                if (alertId) {
                    dismiss(alertId);
                }

                show(alertContainer, alertTypes.danger, alertMessage, alertId);
            }

            function dismiss(alertId) {
                $('#' + alertId).alert("close");
            }

            var ajaxErrorMessage = "There has been an error. Please try again later or contact your system administrator.";

            function onAjaxRequestFail(jqXHR, textStatus, error) {
                showDanger($("#alerts_container"), ajaxErrorMessage);
                console.log({
                    response: jqXHR.responseText,
                    status: jqXHR.status
                });
            }

            return {
                showSuccess: showSuccess,
                showInfo: showInfo,
                showWarning: showWarning,
                showDanger: showDanger,

                dismiss: dismiss,

                onAjaxRequestFail: onAjaxRequestFail
            };
        }

        return AlertsManager;
    }
);

