define('app/apiclients/VehicleMakeApiClient',
    ['jquery'],
    function($) {
        function VehicleMakeApiClient() {

            function getVehicleModels(params) {

                var vehicleMake = params.vehicleMake;
                var vehicleYear = params.vehicleYear;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/vehicle/models",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    "data": $.param({
                        "make": vehicleMake,
                        "year": vehicleYear
                    })
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                getVehicleModels: getVehicleModels
            };
        }

        return VehicleMakeApiClient;
    }
);
