define('app/apiclients/TransactionApiClient',
    ['jquery'],
    function($) {
        function TransactionApiClient() {

            function getTransactionLog(params) {

                var searchFilters = params.searchFilters;
                var onSuccess = params.onSuccess;
                var onFail = params.onFail;

                var settings = {
                    "async": true,
                    "url": "api/transaction",
                    "method": "GET",
                    "headers": {
                        "content-type": "application/json"
                    },
                    //"data": JSON.stringify(searchFilters)
                    "data": $.param(searchFilters)
                };

                console.log("GET request sent to: " + settings.url);

                $.ajax(settings).done(onSuccess).fail(onFail);
            }

            return {
                getTransactionLog: getTransactionLog
            };
        }

        return TransactionApiClient;
    }
);
