// For any third party dependencies, like jQuery, place them in the lib folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
require.config({
    baseUrl: 'js/lib',
    paths: {
        app: '../app'
    },
    // Using this because of problems while loading jquery and/or bootstrap.
    // https://stackoverflow.com/questions/30734296/for-no-reasons-sometimes-jquery-wont-load-with-require-js
    shim: {
        jquery: {
          exports: '$'
        },
        bootstrap: {
            deps: ['jquery']
        }
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
require(['app/viewmodels/LeadLogViewModel']);